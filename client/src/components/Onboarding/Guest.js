
import React from 'react'
import logo from '../../assets/images/redbot.png'
import {
  Box,
  Image,
  Anchor,
  Headline,
  Label,
  Button
} from 'grommet'

const Guest = () => (
  <Box full={true}
    justify='center'
    align='center'>
    <Image src={logo} />
    <Headline size='large'
      align='center'>
      Welcome to Greenbot
    </Headline>
    <Label size='large'>
      Get ready to grow better green
    </Label>
    <Box pad='medium'>
      <Button label='Purchase a Device'
        primary={true} />
      <Label size='large'
        align='center'>
        or
      </Label>
      <Anchor align='center'
        path='/setup'>
        Setup a new device
      </Anchor>
    </Box>
  </Box>
)

export default Guest
