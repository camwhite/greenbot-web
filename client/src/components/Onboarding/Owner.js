
import React from 'react'
import logo from '../../assets/images/redbot.png'
import {
  Box,
  Image,
  Anchor,
  Headline,
  Label
} from 'grommet'
import Device from '../Common/Device'

const Owner = () => (
  <Box full={true}
    justify='center'
    align='center'>
    <Image src={logo} />
    <Headline size='large'
      align='center'>
      No Device Selected
    </Headline>
    <Label size='large'>
      Select one below
    </Label>
    <Box pad='medium'
      justify='center'>
      <Device />
      <Label size='large'
        align='center'>
        or
      </Label>
      <Anchor align='center'
        path='/setup'>
        Setup a new device
      </Anchor>
    </Box>
  </Box>
)

export default Owner
