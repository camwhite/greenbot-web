import React from 'react'
import FlipMove from 'react-flip-move'
import { connect } from 'react-redux'
import { dismiss } from '../../actions/notification'
import {
  Box,
  Notification
} from 'grommet'

const Notifications = ({ dispatch, notifications }) => (
  <Box flex={true}
    justify='end'
    className='notifications'
    size='xlarge'
    pad={{ horizontal: 'xsmall' }}
    style={{
      position: 'fixed',
      bottom: 0,
      right: 0
    }}>
    <Box margin={{ horizontal: 'small' }}>
      <FlipMove duration={300}
        style={{
          maxHeight: '100vh',
          overflowY: 'scroll'
        }}
        onStart={(elem, { parentNode }) => {
          parentNode.scrollTop = parentNode.scrollHeight
        }}
        maintainContainerHeight={true}
        easing='ease-in-out'>
        {notifications.map((notification, index) => {
          const {
            action,
            created_at,
            text,
            status,
            state
          } = notification
          return (
            <Notification full='horizontal'
              key={index}
              size='medium'
              flex={true}
              direction='row'
              style={{ margin: '8px 0' }}
              message={text} 
              status={status} 
              state={state}
              timestamp={created_at}
              closer={true}
              onClose={() => {
                if (action) {
                  action()
                }
                dispatch(dismiss(notification))
              }} />
          )
        })}
      </FlipMove>
    </Box>
  </Box>
)

const mapStateToProps = (state) => {
  const { notifications } = state.pushNotifications
  return {
    notifications
  }
}

export default connect(mapStateToProps)(Notifications)
