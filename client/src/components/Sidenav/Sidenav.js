import React, { Component } from 'react'
import { connect } from 'react-redux'
import Ask from '../Ask/Ask'
import Device from '../Common/Device'
import { toggle } from '../../actions/menu'
import {
  Animate,
  Anchor,
  Sidebar,
  Header,
  Title,
  Menu,
  Box,
  Button,
  DownIcon,
  GrowIcon,
  ClockIcon,
  ConfigureIcon,
  ClusterIcon,
  UpIcon,
  Responsive
} from 'grommet'

class Sidenav extends Component {

  constructor () {
    super()
    this.state = { modalOpen: false, small: false }
  }

  componentDidMount () {
    this.responsive = Responsive.start(this.onResponsive)
  }

  componentWillUnmount () {
    this.responsive.stop()
  }

  onResponsive = (small) => {
    this.setState({ small })
  }

  render () {
    const {
      dispatch,
      isOpen
    } = this.props
    const { small } = this.state

    let closeNode
    if (isOpen && small) {
      closeNode = (
        <Box full='horizontal'
          align='center'>
          <Button plain={true}
            justify='center'
            icon={<DownIcon />}
            onClick={() => dispatch(toggle())} />
        </Box>
      )
    }

    const content = (
      <Box size='auto'
        full={!small}>
        {closeNode}
        <Header pad={{ vertical: 'medium' }}
          direction='column'>
          <Anchor onClick={() => dispatch(toggle())}
            path='/'>
            <Title>
              Greenbot Dashboard
            </Title>
          </Anchor>
          <Device />
        </Header>
        <Box flex={true}
          justify='start'>
          <Menu primary>
            <Anchor path='/conditions'
              onClick={() => dispatch(toggle())}
              style={{ textDecoration: 'none' }}>
              <Box flex={true}
                alignContent='center'
                direction='row'
                responsive={false}>
                <GrowIcon style={{ paddingRight: '8px' }} />
                Current Conditions
              </Box>
            </Anchor>
            <Anchor path='/history'
              onClick={() => dispatch(toggle())}
              style={{ textDecoration: 'none' }}>
              <Box flex={true}
                alignContent='center'
                direction='row'
                responsive={false}>
                <ClockIcon style={{ paddingRight: '8px' }} />
                History
              </Box>
            </Anchor>
            <Anchor path='/settings'
              onClick={() => dispatch(toggle())}
              style={{ textDecoration: 'none' }}>
              <Box flex={true}
                alignContent='center'
                direction='row'
                responsive={false}>
                <ConfigureIcon style={{ paddingRight: '8px' }} />
                Settings
              </Box>
            </Anchor>
            <Anchor path='/setup'
              onClick={() => dispatch(toggle())}
              style={{ textDecoration: 'none' }}>
              <Box flex={true}
                alignContent='center'
                direction='row'
                responsive={false}>
                <ClusterIcon style={{ paddingRight: '8px' }} />
                Setup
              </Box>
            </Anchor>
          </Menu>
        </Box>
        <Box size='auto'
          align='end'>
          <Ask />
        </Box>
      </Box>
    )

    if (!small) {
      return (
        <Sidebar colorIndex='brand'
          style={{ background: '#00AB44' }}
          size='medium'
          fixed={true}>
          {content}
        </Sidebar>
      )
    } else if (isOpen) {
      return (
        <Animate enter={{
          "animation": "slide-down",
          "duration": 200,
          "delay": 0
        }}
        keep={true}>
        <Sidebar colorIndex='brand'
          style={{ background: '#00AB44' }}
          full={false}
          fixed={false}>
          {content}
        </Sidebar>
      </Animate>
      )
    } else {
      return (
        <Box full='horizontal'
          colorIndex='brand'
          style={{ background: '#00AB44' }}
          align='center'>
          <Button plain={true}
            box={true}
            direction='column'
            justify='center'
            icon={<UpIcon />}
            onClick={() => dispatch(toggle())} />
        </Box>
      )
    }
  }

}

const mapStateProps = (state) => {
  const { isOpen } = state.menuToggle

  return {
    isOpen
  }
}

export default connect(mapStateProps)(Sidenav)
