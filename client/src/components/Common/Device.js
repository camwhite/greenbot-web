import React from 'react'
import { connect } from 'react-redux'
import {
  Menu,
  TechnologyIcon,
  AddIcon,
  ActionsIcon,
  Anchor
} from 'grommet'
import { switchDevice } from '../../actions/socket'

const Device = ({ dispatch, currentUser, currentDevice }) => (
  <Menu responsive={true}
    fill={true}
    inline={false}
    primary={false}
    icon={<TechnologyIcon />}
    label={currentDevice ? currentDevice.name ||
        `greenbot-${currentUser.devices.indexOf(currentDevice) !== -1 ?
            currentUser.devices.indexOf(currentDevice) + 1 : currentUser.devices.length + 1 }`
        : 'No device selected'}>
        <Anchor onClick={() => dispatch(switchDevice(null))}
          icon={<AddIcon />}>
          New device
        </Anchor>
        {currentUser.devices.map((device, index) => (
          <Anchor key={device.id}
            icon={<ActionsIcon />}
            onClick={() => !currentDevice || currentDevice.id !== device.id ? dispatch(switchDevice(device)) : null}> 
            {device.name || `greenbot-${index + 1}`}
          </Anchor>
        ))}
      </Menu>
)

const mapStateToProps = (state) => {
  const { currentDevice } = state.deviceSelection
  const { currentUser } = state.authenticationRequest
  return {
    currentDevice,
    currentUser
  }
}

export default connect(mapStateToProps)(Device)
