import React from 'react'
import logo from '../../assets/images/redbot.png'
import './Logo.css'

const Logo = () => (
  <img alt='Greenbot'
    className='logo'
    src={logo} />
)

export default Logo
