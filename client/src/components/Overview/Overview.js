import React from 'react'
import { connect } from 'react-redux'
import { switchDevice } from '../../actions/socket'
import Logo from '../Logo/Logo'
import {
  Box,
  Hero,
  Card,
  Image,
  Anchor,
  Animate,
  Paragraph
} from 'grommet'

const Overview = ({ dispatch, isRequesting, devices }) => {
  if (isRequesting) {
    return (
      <Box flex={true}
        pad='large'
        align='center'
        alignContent='center'
        justify='start'>
        <Animate enter={{
          "animation": "slide-up",
          "duration": 600,
          "delay": 0
        }}>
        <Logo />
      </Animate>
      <Paragraph align='center'>
        Fetching your information, maybe a good time to spark one <span role='img' aria-label='grow'>🌱</span>
      </Paragraph>
    </Box>
    )
  }
  return (
    <Box size='auto'>
      {devices.map(device => {
        const arrayBuffer = new Uint8Array(device.timelapse.value.data).buffer
        const blob = new Blob([ arrayBuffer ], { type: 'image/png' })
        const url = window.URL.createObjectURL(blob)
        return (
          <Box pad={{ vertical: 'medium' }}>
            <Hero background={<Image src={url}
                fit='cover'
                full={true} />}
              size='small'
              backgroundColorIndex='dark'>
              <Box direction='row'
                justify='center'
                align='center'>
                <Box basis='1/2'
                  align='end'
                  pad='medium' />
                <Box basis='1/2'
                  align='start'
                  pad='medium'>
                  <Box colorIndex='grey-2-a'>
                    <Card heading={device.name}
                      description={device.description}
                      label='the latest from'
                      link={<Anchor path='/conditions'
                        onClick={() => dispatch(switchDevice(device))}
                        primary={true}
                        label='See Conditions' />} />
                  </Box>
                </Box>
              </Box>
            </Hero>
          </Box>
        )
      })}
    </Box>
  )
}

const mapStateToProps = (state) => {
  const { isRequesting, devices } = state.userDevicesRequest
  return {
    isRequesting,
    devices
  }
}

export default connect(mapStateToProps)(Overview)
