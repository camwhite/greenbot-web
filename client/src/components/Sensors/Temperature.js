import React from 'react'
import { connect } from 'react-redux'
import {
  Box,
  Heading,
  Meter,
  Value
} from 'grommet'
import Loading from './Loading'

const Temperature = ({ temp }) => {
  if (!temp) {
    return (
      <Loading />
    )
  }
  return (
    <Box style={{ padding: '8px 8%' }}
      align='center'>
      <Meter type='circle'
        label={<Value value={Math.round(temp)}
          min={-15}
          max={70}
          units='&#176;' />}
        value={temp} />
      <Heading align='center'
        margin='medium'>
        Temperature
      </Heading>
    </Box>
  )
}

const mapStateToProps = (state) => {
  const { temp } = state.sensorData
  return {
    temp
  }
}

export default connect(mapStateToProps)(Temperature)
