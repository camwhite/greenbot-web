import React from 'react'
import { connect } from 'react-redux'
import {
  Box,
  Heading,
  Meter,
  Value
} from 'grommet'
import Loading from './Loading'

const Co2 = ({ co2 }) => {
  if (!co2) {
    return (
      <Loading />
    )
  }
  return (
    <Box style={{ padding: '8px 8%' }}
      align='center'>
      <Meter type='circle'
        min={0}
        max={2000}
        label={<Value value={co2}
          units='ppm' />}
        value={co2} />
      <Heading align='center'
        margin='medium'>
        CO<sub>2</sub>
      </Heading>
    </Box>
  )
}

const mapStateToProps = (state) => {
  const { co2 } = state.sensorData
  return {
    co2
  }
}

export default connect(mapStateToProps)(Co2)
