import React from 'react'
import { connect } from 'react-redux'
import {
  Box,
  Heading,
  Meter,
  Value
} from 'grommet'
import Loading from './Loading'

const Humidity = ({ humidity }) => {
  if (!humidity) {
    return (
      <Loading />
    )
  }
  return (
    <Box style={{ padding: '8px 8%' }}
      align='center'>
      <Meter type='circle'
        label={<Value value={Math.round(humidity)}
          units='&#65130;' />}
        value={Math.round(humidity)} />
      <Heading align='center'
        margin='medium'>
        Humidity
      </Heading>
    </Box>
  )
}

const mapStateToProps = (state) => {
  const { humidity } = state.sensorData
  return {
    humidity
  }
}

export default connect(mapStateToProps)(Humidity)
