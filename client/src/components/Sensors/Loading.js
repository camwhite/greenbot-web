
import React, { Component } from 'react'
import {
  Box,
  Meter,
  GrowIcon
} from 'grommet'

class Loading extends Component {

  constructor () {
    super()
    this.state = { loadValue: 0 }
  }

  componentWillMount () {
    this.load = setInterval(() => {
      let { loadValue } = this.state
      if (loadValue < 100) {
        this.setState({ loadValue: loadValue += 1 })
      } else {
        this.setState({ loadValue: 0 })
      }
    }, 50)
  }

  componentWillUnmount () {
    clearInterval(this.load)
  }

  render () {
    const { loadValue } = this.state
    return (
      <Box size='medium'
        pad={{ vertical: 'large' }}
        flex={true}
        justify='center'
        align='center'>
        <Meter type='circle'
          label={<GrowIcon size='large' />}
          value={loadValue} />
      </Box>
    )
  }
}

export default Loading
