import React from 'react'
import { connect } from 'react-redux'
import Logo from '../Logo/Logo'
import {
  Box,
  Image,
  Animate,
  Paragraph
} from 'grommet'

const Timelapse = ({ photo, isNightCycle }) => {
  if (isNightCycle) {
    return (
      <Box flex={true}
        pad='large'
        align='center'
        alignContent='center'
        justify='end'>
        <Animate enter={{
          "animation": "slide-up",
          "duration": 600,
          "delay": 0
        }}>
        <Logo />
        </Animate>
        <Paragraph align='center'>
          It's nighty night, go smoke some indica <span role='img' aria-label='night'>🌑</span>
        </Paragraph>
      </Box>
    )
  }
  if (!photo) {
    return (
      <Box flex={true}
        pad='large'
        align='center'
        alignContent='center'
        justify='end'>
        <Animate enter={{
          "animation": "slide-up",
          "duration": 600,
          "delay": 0
        }}>
        <Logo />
        </Animate>
        <Paragraph align='center'>
          Connecting to the live <span role='img' aria-label='tv'>🌱</span>
        </Paragraph>
      </Box>
    )
  }
  if (this.url) {
    window.URL.revokeObjectURL(this.url)
  }
  const blob = new Blob([ photo ], { type: 'image/png' })
  this.url = window.URL.createObjectURL(blob)
  return (
  <Box pad={{ vertical: 'large' }}>
    <Image full={true}
      size='xlarge'
      style={{ borderRadius: '11px' }}
      src={this.url} />
  </Box>
  )
  }

  const mapStateToProps = (state) => {
    const { photo, isNightCycle } = state.sensorData
    return {
      photo,
      isNightCycle
    }
  }

  export default connect(mapStateToProps)(Timelapse)
