import React from 'react'
import { connect } from 'react-redux'
import {
  Box,
  Form,
  FormField,
  Label,
  NumberInput
} from 'grommet'
import { updateSettings } from '../../actions/settings'
import _debounce from 'lodash/debounce'


const Notifications = ({ dispatch, settings }) => {
  const onNumberChange = _debounce((setting, value) => {
    settings[setting] = value
    dispatch(updateSettings(settings))
  }, 200)
  return (
    <Box>
      <Form pad='medium'>
        <Label>
          Temperature
        </Label>
        <FormField label=' Minimum'>
          <NumberInput min={0}
            value={settings.min_temp}
            onChange={(evt) => onNumberChange('min_temp', evt.target.value)}
            max={100} />
        </FormField>
        <FormField label=' Maximum'>
          <NumberInput min={0}
            onChange={(evt) => onNumberChange('max_temp', evt.target.value)}
            value={settings.max_temp}
            max={100} />
        </FormField>
      </Form>
      <Form pad='medium'>
        <Label>
          CO2
        </Label>
        <FormField label=' Minimum'>
          <NumberInput min={0}
            value={settings.min_co2}
            onChange={(evt) => onNumberChange('min_co2', evt.target.value)}
            step={100}
            max={3000} />
        </FormField>
        <FormField label=' Maximum'>
          <NumberInput min={0}
            value={settings.max_co2}
            onChange={(evt) => onNumberChange('max_co2', evt.target.value)}
            step={100}
            max={100} />
        </FormField>
      </Form>
      <Form pad='medium'>
        <Label>
          Humidity
        </Label>
        <FormField label=' Minimum'>
          <NumberInput min={0}
            value={settings.min_humidity}
            onChange={(evt) => onNumberChange('min_humidity', evt.target.value)}
            max={100} />
        </FormField>
        <FormField label=' Maximum'>
          <NumberInput min={0}
            onChange={(evt) => onNumberChange('max_humidity', evt.target.value)}
            value={settings.max_humidity}
            max={100} />
        </FormField>
      </Form>
    </Box>
  )
}

const mapStateToProps = (state) => {
  const { settings } = state.authenticationRequest.currentUser

  return {
    settings
  }
}

export default connect(mapStateToProps)(Notifications)
