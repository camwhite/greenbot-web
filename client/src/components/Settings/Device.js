import React from 'react'
import { connect } from 'react-redux'
import {
  Form,
  CheckBox,
  Button,
  Box,
  Label,
  ServicesIcon,
  CycleIcon
} from 'grommet'
import { updateSettings } from '../../actions/settings'
import { emit } from '../../actions/socket'

const Device = ({ dispatch, settings, currentDevice }) => (
  <Form pad='medium'>
    <CheckBox toggle={true}
      onChange={(evt) => {
        const isFahrenheit = evt.target.checked
        dispatch(updateSettings({ ...settings, is_fahrenheit: isFahrenheit }))
      }}
      checked={settings.is_fahrenheit}
      reverse={true}
      label='Use Fahrenheit for Temperature'
      id='degrees-select'
      name='degrees' />
    {currentDevice ?
      <Box pad={{ vertical: 'medium' }}>
        <Label>
          Expose <strong>{currentDevice.name}</strong> to a fresh air enviroment and then calibrate
        </Label>
        <Button label='Zero Calibrate CO2'
          icon={<ServicesIcon />}
          critical={true}
          onClick={() => dispatch(emit('sensor:calibrate'))} />
        <Label>
          Unpair <strong>{currentDevice.name}</strong> with your account and delete all data
        </Label>
        <Button label='Reset Device'
          icon={<CycleIcon />}
          critical={true}
          onClick={() => this.consent ? dispatch(emit('device:reset')) : null} />
        <Box pad={{ vertical: 'medium' }}>
          <CheckBox label='I understand this is an adavnced setting'
            onChange={(evt) => this.consent = evt.target.checked} />
        </Box>
      </Box>
      : <Box /> }
    </Form>
)

const mapStateToProps = (state) => {
  const { settings } = state.authenticationRequest.currentUser
  const { currentDevice } = state.deviceSelection

  return {
    settings,
    currentDevice
  }
}

export default connect(mapStateToProps)(Device)
