import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Header, Chart, Title } from 'grommet'
import {
  Axis,
  Marker,
  MarkerLabel,
  Grid,
  HotSpots,
  Base,
  Layers,
  Line
} from 'grommet/components/chart/Chart'
import Info from './Info'
import { setActiveIndex } from '../../actions/sensor'
import * as _debounce from 'lodash/debounce'

class Graph extends Component {

  calculateMax = (values) => {
    const { value } = values.reduce((a, b) => Math.ceil(a.value) > Math.ceil(b.value) ? a : b)
    return Math.ceil(value)
  }

  calculateMin = (values) => {
    const { value } = values.reduce((a, b) => Math.floor(a.value) < Math.floor(b.value) ? a : b)
    return Math.floor(value)
  }

  render () {
    const { dispatch, co2s, index } = this.props

    if (!co2s || co2s.length === 0) {
      return (
        <Header size='xlarge'
          margin={{ vertical: 'large' }}
          pad='xlarge'
          flex={true}
          colorIndex='light-2'
          justify='center'
          align='center'>
          <Title align='center'>
            {!co2s ? 'Select a valid date range' : 'No data available for selected range'}
          </Title>
        </Header>
      )
    }

    const min = this.calculateMin(co2s)
    const max = this.calculateMax(co2s)
    const debounced = _debounce((i) => i ? dispatch(setActiveIndex(i)) : null, 200)

    return (
      <Chart full={true}
        style={{ width: co2s.length * 7, minWidth: '100%' }}>
        <Axis count={max}
          labels={[{ index: 0, label: min }, { index: max - 1, label: max }]}
          vertical={true} />
        <Chart full={true}
          vertical={true}>
          <MarkerLabel count={co2s.length}
            index={index}
            label={<Info createdAt={co2s[index].created_at}
              unit='ppm'
              value={co2s[index].value} />} />
          <Base height='large'
            width='full' />
          <Layers>
            <Grid rows={((max - min) / 100) + 1}
              columns={2} />
            <Line values={[ ...co2s.map(c => c.value) ]}
              activeIndex={index}
              max={max}
              min={min}
              colorIndex='graph-3' />
            <Marker colorIndex='grey-2'
              count={co2s.length}
              vertical={true}
              index={index} />
            <HotSpots count={co2s.length}
              activeIndex={index}
              onActive={debounced} />
          </Layers>
          <Axis count={2} />
        </Chart>
      </Chart>
    )
  }
}

const mapStateToProps = (state) => {
  const { isRequesting, co2s } = state.historyRequest
  const { index } = state.activeIndexSet
  return {
    isRequesting,
    co2s,
    index
  }
}

export default connect(mapStateToProps)(Graph)
