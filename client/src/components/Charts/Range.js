import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getReadingsRange } from '../../actions/sensor'
import {
  Box,
  Form,
  Label,
  FormField,
  Select,
  DateTime
} from 'grommet'
import * as moment from 'moment'

class Range extends Component {

  constructor () {
    super()
    this.state = {}
  }

  render () {
    const { dispatch } = this.props
    const options = [
      { label: '1 Hour', value: moment().subtract(1, 'hours') },
      { label: '1 Day', value: moment().subtract(1, 'days') },
      { label: '1 Week', value: moment().subtract(7, 'days') },
      { label: '1 Month', value: moment().subtract(30, 'days') },
    ]
    return (
      <Box full='horizontal'
        pad={{ vertical: 'large' }}
        justify='between'
        direction='row'>
        <Form pad='medium'>
          <Label htmlFor='quick'
            align='end'>
            Quick Selection
          </Label>
          <FormField>
            <Select options={options}
              id='quick'
              value={this.state.option}
              placeHolder='Choose a Duration'
              onChange={({ option }) => {
                this.setState({ option })
                const endDate = new Date(Date.now()).toISOString()
                const startDate = option.value.toISOString()
                dispatch(getReadingsRange(startDate, endDate))
              }}/>
      </FormField>
      </Form>
        <Form pad='medium'>
          <Label htmlFor='from'
            align='end'>
            From
          </Label>
          <FormField>
            <DateTime id='from'
              name='from'
              value={this.state.startDate || ''}
              onChange={(date) => {
                date = new Date(date).toISOString()
                this.setState({ startDate: date })
                if (this.state.endDate) {
                  dispatch(getReadingsRange(date, this.state.endDate))
                }
              }} />
          </FormField>
        </Form>
        <Form pad='medium'>
          <Label htmlFor='to'
            align='end'>
            To
          </Label>
          <FormField>
            <DateTime id='to'
              name='to'
              value={this.state.endDate || ''}
              onChange={(date) => {
                date = new Date(date).toISOString()
                this.setState({ endDate: date })
                if (this.state.startDate) {
                  dispatch(getReadingsRange(this.state.startDate, date))
                }
              }} />
          </FormField>
        </Form>
      </Box>
    )

  }
}

export default connect()(Range)
