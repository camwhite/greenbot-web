import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Header, Chart, Title } from 'grommet'
import {
  Axis,
  Marker,
  MarkerLabel,
  Grid,
  HotSpots,
  Base,
  Layers,
  Line
} from 'grommet/components/chart/Chart'
import Info from './Info'
import { setActiveIndex } from '../../actions/sensor'
import * as _debounce from 'lodash/debounce'

class Humidity extends Component {

  calculateMax = (values) => {
    const { value } = values.reduce((a, b) => Math.ceil(a.value) > Math.ceil(b.value) ? a : b)
    return Math.ceil(value)
  }

  calculateMin = (values) => {
    const { value } = values.reduce((a, b) => Math.floor(a.value) < Math.floor(b.value) ? a : b)
    return Math.floor(value)
  }

  render () {
    const { dispatch, humidities, index } = this.props

    if (!humidities || humidities.length === 0) {
      return (
        <Header size='xlarge'
          margin={{ vertical: 'large' }}
          pad='xlarge'
          flex={true}
          colorIndex='light-2'
          justify='center'
          align='center'>
          <Title align='center'>
            {!humidities ? 'Select a valid date range' : 'No data available for selected range'}
          </Title>
        </Header>
      )
    }

    const minHumidity = this.calculateMin(humidities)
    const maxHumidity = this.calculateMax(humidities)
    const debounced = _debounce((i) => i ? dispatch(setActiveIndex(i)) : null, 200)

    return (
      <Chart full={true}
        style={{ width: humidities.length * 7, minWidth: '100%' }}>
        <Axis count={maxHumidity}
          labels={[{ index: 0, label: minHumidity }, { index: maxHumidity - 1, label: maxHumidity }]}
          vertical={true} />
        <Chart full={true}
          vertical={true}>
          <MarkerLabel count={humidities.length}
            index={index}
            label={<Info createdAt={humidities[index].created_at}
              unit='%'
              value={humidities[index].value} />} />
          <Base height='large'
            width='full' />
          <Layers>
            <Grid rows={maxHumidity - minHumidity + 1}
              columns={2} />
            <Line values={[ ...humidities.map(t => t.value) ]}
              activeIndex={index}
              max={maxHumidity}
              min={minHumidity}
              colorIndex='graph-1' />
            <Marker colorIndex='grey-2'
              count={humidities.length}
              vertical={true}
              index={index} />
            <HotSpots count={humidities.length}
              activeIndex={index}
              onActive={debounced} />
          </Layers>
          <Axis count={2} />
        </Chart>
      </Chart>
    )
  }
}

const mapStateToProps = (state) => {
  const { isRequesting, humidities } = state.historyRequest
  const { index } = state.activeIndexSet
  return {
    isRequesting,
    humidities,
    index
  }
}

export default connect(mapStateToProps)(Humidity)
