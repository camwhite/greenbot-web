import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getTimelapseImage } from '../../actions/sensor'
import {
  Box,
  Layer,
  Image,
  Label,
  Timestamp,
  SpinningIcon,
  Value,
  Responsive
} from 'grommet'

class Info extends Component {

  constructor () {
    super()
    this.state = { modalOpen: false, small: false }
  }

  componentDidMount () {
    this.responsive = Responsive.start(this.onResponsive)
  }

  componentWillUnmount () {
    this.responsive.stop()
  }

  onResponsive = (small) => {
    this.setState({ small })
  }

  componentWillUpdate (nextProps) {
    if (nextProps.createdAt && nextProps.createdAt !== this.props.createdAt) {
      this.props.dispatch(getTimelapseImage(nextProps.createdAt))
    }
  }

  render () {
    const { err, createdAt, value, isRequesting, timelapse, unit } = this.props
    if (!value || !createdAt) {
      return (
        <Box size='medium'>
        </Box>
      )
    }
    if (err) {
      return (
        <Box size='medium'
          responsive={false}
          direction='row'>
          <Box pad='small'
            align='center'
            direction='column'>
            <Value units={unit} value={parseFloat(value).toFixed(2)} />
            <Timestamp value={createdAt} />
          </Box>
          <Box size='small'
            pad='small'
            colorIndex='critical'
            flex={true}
            justify='center'
            align='center'>
            Image not found.. was it the night cycle?
          </Box>
        </Box>
      )
    }
    if (isRequesting || !timelapse) {
      return (
        <Box size='medium'
          responsive={false}
          direction='row'>
          <Box pad='small'
            align='center'
            direction='column'>
            <Value units={unit} value={parseFloat(value).toFixed(2)} />
            <Timestamp value={createdAt} />
          </Box>
          <Box size='small'
            flex={true}
            justify='center'
            align='center'>
            <SpinningIcon size='small' />
          </Box>
        </Box>
      )
    }

    const arrayBuffer = new Uint8Array(timelapse.value.data).buffer
    const blob = new Blob([ arrayBuffer ], { type: 'image/png' })
    const url = window.URL.createObjectURL(blob)

    if (this.state.small) {
      return (
        <Box size='small'
          basis='xsmall'
          direction='row'>
          <Box pad='small'
            align='center'
            direction='column'>
            <Value units={unit} value={parseFloat(value).toFixed(2)} />
            <Timestamp value={createdAt} />
          </Box>
          <Box size='small'
            justify='center'
            onClick={() => this.setState({ modalOpen: true })}>
            <Label size='small'
              align='center'>
              Tap for image
            </Label>
          </Box>
          {this.state.modalOpen ?
              <Layer closer={true}
                align='center'
                onClose={() => this.setState({ modalOpen: false })}>
                <Box full={true}
                  justify='center'>
                  <Image src={url} fit='cover' />
                </Box>
              </Layer>
              : ''}
            </Box>
      )
    }

    return (
      <Box size='auto'
        basis='xsmall'
        direction='row'>
        <Box pad='small'
          align='center'
          direction='column'>
          <Value units={unit} value={parseFloat(value).toFixed(2)} />
          <Timestamp value={createdAt} />
        </Box>
        <Box size='small'
          pad='none'
          onClick={() => this.setState({ modalOpen: true })}>
          <Image fit='cover'
            src={url} />
        </Box>
        {this.state.modalOpen ?
            <Layer closer={true}
              align='center'
              onClose={() => this.setState({ modalOpen: false })}>
              <Box full={true}
                justify='center'>
                <Image src={url}
                  fit='contain' />
              </Box>
            </Layer>
            : ''}
          </Box>
    )
  }
}

const mapStateToProps = (state) => {
  const { err, timelapse, isRequesting } = state.timelapseImageRequest
  return {
    timelapse,
    isRequesting,
    err
  }
}

export default connect(mapStateToProps)(Info)
