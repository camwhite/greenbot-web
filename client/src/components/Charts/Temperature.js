import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Header, Chart, Title } from 'grommet'
import {
  Axis,
  Marker,
  MarkerLabel,
  Grid,
  HotSpots,
  Base,
  Layers,
  Line
} from 'grommet/components/chart/Chart'
import Info from './Info'
import { setActiveIndex } from '../../actions/sensor'
import * as _debounce from 'lodash/debounce'

class Graph extends Component {

  calculateMax = (values) => {
    const { value } = values.reduce((a, b) => Math.ceil(a.value) > Math.ceil(b.value) ? a : b)
    return Math.ceil(value)
  }

  calculateMin = (values) => {
    const { value } = values.reduce((a, b) => Math.floor(a.value) < Math.floor(b.value) ? a : b)
    return Math.floor(value)
  }

  render () {
    const { dispatch, temperatures, index } = this.props

    if (!temperatures || temperatures.length === 0) {
      return (
        <Header size='xlarge'
          margin={{ vertical: 'large' }}
          pad='xlarge'
          flex={true}
          colorIndex='light-2'
          justify='center'
          align='center'>
          <Title align='center'>
            {!temperatures ? 'Select a valid date range' : 'No data available for selected range'}
          </Title>
        </Header>
      )
    }

    const minTemp = this.calculateMin(temperatures)
    const maxTemp = this.calculateMax(temperatures)
    const debounced = _debounce((i) => i ? dispatch(setActiveIndex(i)) : null, 200)

    return (
      <Chart full={true}
        style={{ width: temperatures.length * 7, minWidth: '100%' }}>
        <Axis count={maxTemp}
          labels={[{ index: 0, label: minTemp }, { index: maxTemp - 1, label: maxTemp }]}
          vertical={true} />
        <Chart full={true}
          vertical={true}>
          <MarkerLabel count={temperatures.length}
            index={index}
            label={<Info createdAt={temperatures[index].created_at}
              unit='&#176;'
              value={temperatures[index].value} />} />
          <Base height='large'
            width='full' />
          <Layers>
            <Grid rows={maxTemp - minTemp + 1}
              columns={2} />
            <Line values={[ ...temperatures.map(t => t.value) ]}
              activeIndex={index}
              max={maxTemp}
              min={minTemp}
              colorIndex='graph-2' />
            <Marker colorIndex='grey-2'
              count={temperatures.length}
              vertical={true}
              index={index} />
            <HotSpots count={temperatures.length}
              activeIndex={index}
              onActive={debounced} />
          </Layers>
          <Axis count={2} />
        </Chart>
      </Chart>
    )
  }
}

const mapStateToProps = (state) => {
  const { isRequesting, temperatures } = state.historyRequest
  const { index } = state.activeIndexSet
  return {
    isRequesting,
    temperatures,
    index
  }
}

export default connect(mapStateToProps)(Graph)
