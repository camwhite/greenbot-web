import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Box, Button } from 'grommet'

class Export extends Component {

  onDownload = () => {
    let { co2s, temperatures, humidities } = this.props
    const csvData = [[ 'Co2', 'Temperature', 'Humidity' ]]

    for (let i = 0; i < co2s.length; i++) {
      // Stop gap measure for time being due to synchronization
      if (!co2s[i] || !temperatures[i] || !humidities[i]) {
        return
      }
      const row = [ co2s[i].value, temperatures[i].value, humidities[i].value ]
      csvData.push(row)
    }

    const csv = `${csvData.map(row =>  row.join(',')).join(`\n`)}`
    const blob = new Blob([ csv ], { type: 'text/csv;charset=utf-8;' })
    const url = window.URL.createObjectURL(blob)
    const link = document.createElement('a')

    link.href = url
    link.setAttribute('download', 'grow-data.csv')
    link.click()
  }

  render () {
    const { co2s, temperatures, humidities } = this.props
    if (!co2s || !temperatures || !humidities) {
      return <Box />
    }
    return (
      <Box full='horizontal'
        direction='row'
        justify='center'>
        <Button accent={false}
          label='Export to CSV'
          onClick={this.onDownload} />
      </Box>
    )
  }
}

const mapStateToProps = (state) => {
  const { co2s, temperatures, humidities } = state.historyRequest
  return {
    co2s,
    temperatures,
    humidities
  }
}

export default connect(mapStateToProps)(Export)
