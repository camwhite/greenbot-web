import React from 'react'
import { connect } from 'react-redux'
import {
  Box,
  Paragraph,
  Label,
  Form,
  FormField,
  TextInput,
  Footer,
  Button
} from 'grommet'
import { updateDevice } from '../../actions/device'

const Pair = ({ dispatch, currentUser, currentDevice }) => {
  if (!currentDevice) {
    return (
      <Box size='xlarge'
        align='center'>
        <Paragraph align='center'
          size='large'>
          Plug in power source and ethernet cable. Then, 
          point the camera on your device at the QR code to pair.
        </Paragraph>
				{/* eslint-disable-next-line react/no-danger-with-children */}
        <div style={{ width: '100%' }} dangerouslySetInnerHTML={{ __html: currentUser.qr_code }}></div>
        <Paragraph align='center'>
          You have {currentUser.devices ? currentUser.devices.length + ' ' : 'no '} 
          devices currently tied to your account.
        </Paragraph>
      </Box>
    )
  }

  this.name = currentDevice.name
  this.description = currentDevice.description

  return (
    <Box size='xlarge'
      align='center'>
      <Paragraph size='xlarge'
        align='center'>
        This device has been succesfully paired with your account.
      </Paragraph>
      <Label align='center'>
        Would you like to name and describe it's purpose?
      </Label>
      <Form onSubmit={(evt) => {
        evt.preventDefault()
        const info = { name: this.name, description: this.description }
        dispatch(updateDevice(info))
      }}
      compact={true}>
      <FormField label='Name'
        htmlFor='name-input'>
        <TextInput id='name-input'
          defaultValue={this.name}
          onDOMChange={(evt) => this.name = evt.target.value} />
      </FormField>
      <FormField label='Description'
        htmlFor='description-input'>
        <TextInput id='description-input'
          defaultValue={this.description}
          onDOMChange={(evt) => this.description = evt.target.value} />
      </FormField>
      <Footer pad={{ vertical: 'medium' }}>
        <Button label='Submit'
          type='submit'
          primary={true} />
      </Footer>
    </Form>
    <Paragraph align='center'>
      You can now move on to the next steps.
    </Paragraph>
  </Box>
  )
}

const mapStateToProps = (state) => {
  const { currentUser } = state.authenticationRequest
  const { currentDevice } = state.deviceSelection
  return {
    currentUser,
    currentDevice
  }
}

export default connect(mapStateToProps)(Pair)
