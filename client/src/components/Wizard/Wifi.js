import React, { Component } from 'react'
import { connect } from 'react-redux'
import { emit, sync, unsync } from '../../actions/socket'
import {
  Box,
  Headline,
  Paragraph,
  Footer,
  Button,
  Form,
  PasswordInput,
  List,
  ListItem,
  LockIcon,
  SpinningIcon
} from 'grommet'

class Wifi extends Component {

  componentWillMount () {
    this.props.dispatch(sync('wifi:scan'))
    this.props.dispatch(sync('wifi:success'))
    this.props.dispatch(sync('wifi:failure'))
    this.props.dispatch(emit('wifi:enable', true))
  }

  componentWillUnmount () {
    this.props.dispatch(unsync('wifi:scan'))
    this.props.dispatch(unsync('wifi:success'))
    this.props.dispatch(unsync('wifi:failure'))
  }

  render () {
    const { dispatch, networks, output } = this.props
    if (!networks) {
      return (
        <Box full={true}
          flex={true}
          justify='center'
          align='center'>
          <SpinningIcon size='xlarge' />
        </Box>
      )
    }
    return (
      <Box align='center'
        basis='full'>
        <Headline align='center'>
          Select your wireless network
        </Headline>
        <List selectable={true}
          onSelect={(index) => this.setState({ selectedNetwork:  networks[index]})}>
          {networks.map((network, index) => {
            return (
              <ListItem justify='between'
                responsive={false}
                pad='medium'
                key={index}>
                <span style={{ padding: '0 24px' }}>
                  {network.ssid}
                </span>
                <span className='secondary'>
                  <Box direction='row'
                    align='center'
                    justify='center'>
                    <LockIcon />
                    {network.security[0]}
                  </Box>
                </span>
              </ListItem>
            )
          })}
        </List>
        {this.state ? 
          <Box justify='center'
            size='full'
            align='center'>
            <Paragraph align='center'>
              Enter the password for {this.state.selectedNetwork.ssid} below. 
              This information is never stored on our servers and is encrypted during transmission.
            </Paragraph>
            <Form onSubmit={(evt) => {
              const { ssid } = this.state.selectedNetwork
              evt.preventDefault()
              dispatch(emit('wifi:info', {
                ssid,
                psk: this.psk
              }))
            }}
            compact={true}>
              <PasswordInput id='wifi-input'
                onChange={(evt) => this.psk = evt.target.value} />
              <Footer pad={{ vertical: 'medium' }}>
                <Button label='Connect'
                  type='submit'
                  primary={true} />
              </Footer>
          </Form>
          <Paragraph>
            {output ? output : ''}
          </Paragraph>
        </Box>
        :
        <Paragraph>
          Don't see your network? Find out why here.
        </Paragraph>
      }
      </Box>
    )
  }
}

const mapStateToProps = (state) => {
  const { networks, output } = state.wifiConfig
  return {
    networks,
    output
  }
}

export default connect(mapStateToProps)(Wifi)
