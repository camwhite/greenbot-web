import React from 'react'
import { connect } from 'react-redux'
import { ask } from '../../actions/notification'
import Logo from '../Logo/Logo'
import {
  Box,
  Form,
  FormField,
  TextInput,
  Animate
} from 'grommet'

const Ask = ({ dispatch, isRequesting }) => {
  if (isRequesting) {
    return (
      <Box flex={true}
        pad='large'
        align='center'
        alignContent='center'
        justify='end'>
        <Animate enter={{
          "animation": "slide-up",
          "duration": 600,
          "delay": 0
        }}>
          <Logo />
        </Animate>
      </Box>
    )
  }
  return (
    <Form onSubmit={(evt) => {
        evt.preventDefault()
        dispatch(ask(this.query))
      }}
      pad='small'
      compact={false}>
      <FormField label='Have a Question?'
        htmlFor='ask-input'>
        <TextInput id='ask-input'
          placeHolder='Try Asking Greenbot'
          onDOMChange={(evt) => this.query = evt.target.value} />
      </FormField>
    </Form>
  )
}

const mapStateToProps = (state) => {
  const { isRequesting } = state.pushNotifications
  return {
    isRequesting
  }
}

export default connect(mapStateToProps)(Ask)
