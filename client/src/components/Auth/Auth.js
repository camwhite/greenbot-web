import React from 'react'
import { connect } from 'react-redux'
import { signup, login } from '../../actions/auth'
import Logo from '../Logo/Logo'
import {
  Box,
  LoginForm,
  Tabs,
  Tab
} from 'grommet'

const Auth = ({ dispatch, err }) => (
  <Box flex={true}
    justify='center'
    alignContent='center'
    full={true}>
    <Tabs justify='center'
      responsive={false}>
      <Tab title='Sign Up'>
        <Box align='center'>
          <LoginForm className='signup'
            title='Greenbot'
            secondaryText='Sign up to start your setup'
            logo={<Logo />}
            rememberMe={true}
            errors={err ? [ err.message ] : []}
            onSubmit={(creds) => dispatch(signup(creds))} />
        </Box>
      </Tab>
      <Tab title='Log In'>
        <Box align='center'>
          <LoginForm className='login'
            title='Greenbot'
            secondaryText='Log in to get growing'
            logo={<Logo />}
            rememberMe={true}
            errors={err ? [ err.message ] : []}
            onSubmit={(creds) => dispatch(login(creds))} />
        </Box>
      </Tab>
    </Tabs>
  </Box>
)

const mapStateToProps = (state) => {
  const { err } = state.authenticationRequest
  return {
    err
  }
}

export default connect(mapStateToProps)(Auth)
