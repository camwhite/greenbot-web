export const userDevicesRequest = (state = {}, action) => {
  switch (action.type) {
    case 'USER_DEVICES_REQUEST' :
      return {
        ...state,
        isRequesting: true
      }
    case 'USER_DEVICES_SUCCESS' :
      return {
        ...state,
        devices: action.devices,
        isRequesting: false
      }
    case 'USER_DEVICES_FAILURE' :
      return {
        ...state,
        err: action.err,
        isRequesting: false
      }
    default :
      return state
  }
}
