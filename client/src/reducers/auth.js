export const authenticationRequest = (state = {}, action) => {
  switch (action.type) {
    case 'CURRENT_USER_REQUEST' :
      return {
        ...state,
        isRequesting: true
      }
    case 'CURRENT_USER_SUCCESS' :
      return {
        ...state,
        currentUser: action.currentUser,
        isRequesting: false,
        isAuthenticated: true
      }
    case 'CURRENT_USER_FAILURE' :
      return {
        ...state,
        err: action.err,
        isRequesting: false,
        isAuthenticated: false
      }
    default :
      return state
  }
}
