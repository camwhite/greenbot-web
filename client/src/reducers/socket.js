export const sensorData = (state={}, action) => {
  switch (action.type) {
    case 'SENSOR_NIGHT_EVENT' :
      return {
        ...state,
        isNightCycle: true
      }
    case 'SENSOR_CO2_EVENT' :
      return {
        ...state,
        co2: action.data
      }
    case 'SENSOR_CAMERA_EVENT' :
      return {
        ...state,
        photo: action.data,
        isNightCycle: false
      }
    case 'SENSOR_DHT_EVENT' :
      return {
        ...state,
        temp: action.data.temperature,
        humidity: action.data.humidity
      }
    case 'DEVICE_SWITCH_EVENT' :
      return {
        ...state,
        isNightCycle: null,
        photo: null,
        co2: null,
        temp: null,
        humidity: null
      }
    default :
      return state
  }
}

export const wifiConfig = (state={}, action) => {
  switch (action.type) {
    case 'WIFI_SCAN_EVENT' :
      return {
        ...state,
        networks: action.data
      }
    case 'WIFI_SUCCESS_EVENT' :
      return {
        ...state,
        output: action.data
      }
    case 'WIFI_FAILURE_EVENT' :
      return {
        ...state,
        output: action.data
      }
    default :
      return state
  }
}

export const historyData = (state={temperatures: [], humidities: []}, action) => {
  switch (action.type) {
    case 'HISTORY_TEMPERATURES_EVENT' :
      return {
        ...state,
        temperatures: [ ...action.data ]
      }
    case 'HISTORY_HUMIDITIES_EVENT' :
      return {
        ...state,
        humidities: [ ...action.data ]
      }
    default :
      return state
  }
}

export const deviceSelection = (state={}, action) => {
  switch (action.type) {
    case 'DEVICE_SWITCH_EVENT' :
      return {
        ...state,
        currentDevice: action.data
      }
    default :
      return state
  }
}
