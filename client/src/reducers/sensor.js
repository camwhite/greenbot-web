export const historyRequest = (state = {}, action) => {
  switch (action.type) {
    case 'SENSOR_READING_REQUEST' :
      return {
        ...state,
        isRequesting: true
      }
    case 'SENSOR_READING_SUCCESS' :
      return {
        ...state,
        temperatures: action.temperatures,
        humidities: action.humidities,
        co2s: action.co2s,
        isRequesting: false
      }
    case 'SENSOR_READING_FAILURE' :
      return {
        ...state,
        err: action.err,
        isRequesting: false
      }
    default :
      return state
  }
}

export const timelapseImageRequest = (state = {}, action) => {
  switch (action.type) {
    case 'TIMELAPSE_READING_REQUEST' :
      return {
        ...state,
        isRequesting: true
      }
    case 'TIMELAPSE_READING_SUCCESS' :
      return {
        ...state,
        timelapse: action.timelapse,
        isRequesting: false,
        err: null
      }
    case 'TIMELAPSE_READING_FAILURE' :
      return {
        ...state,
        err: action.err,
        isRequesting: false
      }
    default :
      return state
  }
}

export const activeIndexSet = (state = {}, action) => {
  switch (action.type) {
    case 'SET_ACTIVE_INDEX' :
      return {
        ...state,
        index: action.index
      }
    default :
      return state
  }
}
