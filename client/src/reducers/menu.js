export const menuToggle = (state={}, action) => {
  switch (action.type) {
    case 'MENU_TOGGLE_EVENT' :
      return {
        ...state,
        isOpen: action.isOpen
      }
    default :
      return state
  }
}
