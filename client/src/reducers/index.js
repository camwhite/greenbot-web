import { combineReducers } from 'redux'
import { authenticationRequest } from './auth'
import { pushNotifications } from './notification'
import { menuToggle } from './menu'
import { userDevicesRequest } from './device'
import { updateSettingsRequest } from './settings'
import { historyRequest, timelapseImageRequest, activeIndexSet } from './sensor'
import { sensorData, historyData, wifiConfig, deviceSelection } from './socket'

export const makeRootReducer = () => {
  return combineReducers({
    authenticationRequest,
    pushNotifications,
    userDevicesRequest,
    sensorData,
    historyData,
    wifiConfig,
    deviceSelection,
    menuToggle,
    historyRequest,
    timelapseImageRequest,
    activeIndexSet,
    updateSettingsRequest
  })
}

export default makeRootReducer
