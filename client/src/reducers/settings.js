export const updateSettingsRequest = (state = {}, action) => {
  switch (action.type) {
    case 'UPDATE_SETTINGS_REQUEST' :
      return {
        ...state,
        isRequesting: true
      }
    case 'UPDATE_SETTINGS_SUCCESS' :
      return {
        ...state,
        settings: action.settings,
        isRequesting: false
      }
    case 'UPDATE_SETTINGS_FAILURE' :
      return {
        ...state,
        err: action.err,
        isRequesting: false
      }
    default :
      return state
  }
}
