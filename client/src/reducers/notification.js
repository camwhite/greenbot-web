export const pushNotifications = (state = { notifications: [] }, action) => {
  switch (action.type) {
    case 'NOTIFICATION_REQUEST_EVENT' :
      return {
        ...state,
        isRequesting: true
      }
    case 'NOTIFICATION_PUSH_EVENT' :
      return {
        ...state,
        notifications: action.notifications,
        isRequesting: false
      }
    case 'NOTIFICATION_PULL_EVENT' :
      return {
        ...state,
        notifications: action.notifications
      }
    default :
      return {
        ...state
      } 
  }
}
