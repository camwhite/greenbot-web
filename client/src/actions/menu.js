export const toggle = () => (dispatch, getState) => {
  let { isOpen } = getState().menuToggle
  isOpen = !isOpen
  dispatch({
    type: 'MENU_TOGGLE_EVENT',
    isOpen
  })
}
