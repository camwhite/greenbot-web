import { showNotification } from '../registerServiceWorker'
import { callApi } from '../utils/api'
import { toggle } from './menu'

// Notify action
export const notify = (notification) => (dispatch, getState) => {
  let { notifications } = getState().pushNotifications
  notifications = [ ...notifications, notification ]

  if (document.hasFocus()) {
    dispatch({
      type: 'NOTIFICATION_PUSH_EVENT',
      notifications
    })
  } else {
    showNotification(notification.text)
  }
}

export const ask = (query) => (dispatch, getState) => {
  let { notifications } = getState().pushNotifications
  dispatch({
    type: 'NOTIFICATION_REQUEST_EVENT'
  })

  return callApi('users/ask', {
    method: 'post',
    body: JSON.stringify({ query })
  })
    .then(response => {
      dispatch(toggle())

      response.created_at = new Date()
      notifications = [ ...notifications, response ]

      dispatch({
        type: 'NOTIFICATION_PUSH_EVENT',
        notifications
      })
    })

}

export const dismiss = (notification) => (dispatch, getState) => {
  let { notifications } = getState().pushNotifications

  const index = notifications.indexOf(notification)
  notifications.splice(index, 1)
  notifications = [ ...notifications ]

  dispatch({
    type: 'NOTIFICATION_PULL_EVENT',
    notifications
  })
}
