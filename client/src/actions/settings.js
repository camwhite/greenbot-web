import { updateUserSettings } from './auth'
import { callApi } from '../utils/api'

export const updateSettings = (settings) => (dispatch, getState) => {
  dispatch({
    type: 'UPDATE_SETTINGS_REQUEST'
  })

  return callApi(`settings/${settings.id}`, { method: 'put', body: JSON.stringify(settings) })
    .then(settings => {
      dispatch({
        type: 'UPDATE_SETTINGS_SUCCESS',
        settings
      })
      dispatch(updateUserSettings(settings))
    })
    .catch(err => {
      dispatch({
        type: 'UPDATE_SETTINGS_FAILURE',
        err
      })
    })
}

