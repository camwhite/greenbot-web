import { updateUser } from './auth'
import { getIdToken } from '../utils/auth'
import { notify } from './notification'
import { removeDevice } from './device'
import * as io from 'socket.io-client'

let socket = null

export const connect = () => (dispatch, getState) => {
  if (socket !== null) return
  socket = io.connect(process.env.NODE_ENV === 'production' ? '' : 'localhost:3001', {
    transports: [ 'websocket', 'polling' ],
    query: `token=${getIdToken()}`
  })
  socket.on('connect', () => {
    const { currentDevice } = getState().deviceSelection
    if (currentDevice) {
      socket.emit('join', currentDevice.id)
    }
  })
  socket.on('reset:success', () => {
    dispatch(removeDevice())
  })
  socket.on('sensor:notify', (data) => {
    if (!document.hasFocus()) return
    const notification = {
      ...data,
      created_at: new Date(data.created_at),
      status: 'critical',
      text: `Something needs your attention, ${data.sensor} is reporting ${data.value} 👀`
    }
    dispatch(notify(notification))
  })
}

export const sync = (evt) => (dispatch, getState) => {
  const { settings } = getState().authenticationRequest.currentUser
  const type = evt.toUpperCase()
    .split(':')
    .join('_')

  socket.on(evt, (data) => {
    if (evt === 'device:switch') {
      dispatch(updateUser(null, data))
      dispatch(switchDevice(data))
      return
    }
    if (evt === 'sensor:dht' && settings.is_fahrenheit) {
      data.temperature = data.temperature * 1.8 + 32
    }

    dispatch({
      type: type + '_EVENT',
      data
    })
  })
}

export const emit = (evt ,payload) => (dispatch, getState) => {
  socket.emit(evt, payload)
}

export const unsync = (evt) => (dispatch, getState) => {
  socket.removeListener(evt)
}

export const switchDevice = (device) => (dispatch, getState) => {
  const { currentDevice } = getState().deviceSelection
  if (currentDevice) {
    socket.emit('leave', currentDevice.id)
  }
  if (device) {
    socket.emit('join', device.id)
  }
  dispatch({
    type: 'DEVICE_SWITCH_EVENT',
    data: device
  })
}
