import { callApi } from '../utils/api'

export const getReadingsRange = (startDate, endDate) => (dispatch, getState) => {
  const { settings } = getState().authenticationRequest.currentUser
  const { currentDevice } = getState().deviceSelection
  dispatch({
    type: 'SENSOR_READING_REQUEST'
  })

  const apiUrl = `sensors/readings?startDate=${startDate}&endDate=${endDate}&deviceId=${currentDevice.id}`
  return callApi(apiUrl, {
    method: 'get'
  })
    .then(({ temperatures, humidities, co2s }) => {
      if (settings.is_fahrenheit) {
        temperatures = temperatures.map(t => {
          return { ...t, value: t.value * 1.8 + 32 }
        })
      }
      dispatch(setActiveIndex(0))
      dispatch({
        type: 'SENSOR_READING_SUCCESS',
        temperatures,
        humidities,
        co2s
      })
    })
    .catch(err => {
      dispatch({
        type: 'SENSOR_READING_FAILURE',
        err
      })
    })
}

export const getTimelapseImage = (date) => (dispatch, getState) => {
  const { currentDevice } = getState().deviceSelection
  dispatch({
    type: 'TIMELAPSE_READING_REQUEST'
  })

  return callApi(`sensors/timelapse?createdAt=${date}&deviceId=${currentDevice.id}`)
    .then((timelapse) => {
      dispatch({
        type: 'TIMELAPSE_READING_SUCCESS',
        timelapse
      })
    })
    .catch(err => {
      dispatch({
        type: 'TIMELAPSE_READING_FAILURE',
        err
      })
    })
}

export const setActiveIndex = (index) => (dispatch, getState) => {
  dispatch({
    type: 'SET_ACTIVE_INDEX',
    index
  })
}
