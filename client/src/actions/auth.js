import { callApi } from '../utils/api'
import { connect as socket } from './socket'
import {
  setSessionIdToken,
  setLocalIdToken,
  authenticate,
  fetchCurrentUser
} from '../utils/auth'

// Sign up action
export const signup = (credentials) => (dispatch, getState) => {
  dispatch({
    type: 'CURRENT_USER_REQUEST'
  })

  return authenticate('signup', credentials)
    .then(currentUser => {
      if (credentials.rememberMe) {
        setLocalIdToken(currentUser.id_token) // stores the token
      } else {
        setSessionIdToken(currentUser.id_token) // stores the token
      }

      dispatch(socket())
      dispatch({
        type: 'CURRENT_USER_SUCCESS',
        currentUser
      })
    })
    .catch(err => {
      dispatch({
        type: 'CURRENT_USER_FAILURE',
        err
      })
    })
}

// Login up action
export const login = (credentials) => (dispatch, getState) => {
  dispatch({
    type: 'CURRENT_USER_REQUEST'
  })

  return authenticate('login', credentials)
    .then(currentUser => {
      if (credentials.rememberMe) {
        setLocalIdToken(currentUser.id_token) // stores the token
      } else {
        setSessionIdToken(currentUser.id_token) // stores the token
      }

      dispatch(socket())
      dispatch({
        type: 'CURRENT_USER_SUCCESS',
        currentUser
      })
    })
    .catch(err => {
      dispatch({
        type: 'CURRENT_USER_FAILURE',
        err
      })
    })
}

// Get current user action
export const getCurrentUser = () => (dispatch, getState) => {
  const { currentUser } = getState().authenticationRequest
  if (currentUser) {
    return dispatch({
      type: 'CURRENT_USER_SUCCESS',
      currentUser
    })
  }

  dispatch({
    type: 'CURRENT_USER_REQUEST'
  })

  return fetchCurrentUser()
    .then(currentUser => {
      dispatch({
        type: 'CURRENT_USER_SUCCESS',
        currentUser
      })
    })
    .catch(err => {
      dispatch({
        type: 'CURRENT_USER_FAILURE',
        err: err
      })
    })
}

// Update current user action
export const updateUser = (update, device=null) => (dispatch, getState) => {
  const { currentUser } = getState().authenticationRequest
  if (device !== null && currentUser.devices.indexOf(device) === -1) {
    currentUser.devices = [ ...currentUser.devices, device ]
    return dispatch({
      type: 'CURRENT_USER_SUCCESS',
      currentUser
    })
  }

  dispatch({
    type: 'CURRENT_USER_REQUEST'
  })

  return callApi('api/users', { method: 'put', body:  JSON.stringify(update)})
    .then(currentUser => {
      dispatch({
        type: 'CURRENT_USER_SUCCESS',
        currentUser
      })
    })
    .catch(err => {
      dispatch({
        type: 'CURRENT_USER_FAILURE',
        err: err
      })
    })
}

export const updateUserSettings = (settings) => (dispatch, getState) => {
  const { currentUser } = getState().authenticationRequest
  currentUser.settings = settings

 return  dispatch({
    type: 'CURRENT_USER_SUCCESS',
    currentUser
  })
}
