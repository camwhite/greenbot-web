import { callApi } from '../utils/api'

export const getUserDevices = () => (dispatch, getState) => {
  const { devices } = getState().userDevicesRequest
  if (devices) {
    return dispatch({
      type: 'USER_DEVICES_SUCCESS',
      devices
    })
  }

  dispatch({
    type: 'USER_DEVICES_REQUEST'
  })

  return callApi(`devices/all`, { method: 'get' })
    .then(devices => {
      dispatch({
        type: 'USER_DEVICES_SUCCESS',
        devices
      })
    })
    .catch(err => {
      dispatch({
        type: 'USER_DEVICES_FAILURE',
        err
      })
    })
}

export const updateDevice = (info) => (dispatch, getState) => {
  let { currentDevice } = getState().deviceSelection

  return callApi(`devices/${currentDevice.id}`, { method: 'put', body: JSON.stringify(info) })
    .then(device => {
      const { currentUser } = getState().authenticationRequest
      const index = currentUser.devices.findIndex(d => d.id === currentDevice.id)
      currentUser.devices[index] = device
      dispatch({
        type: 'CURRENT_USER_SUCCESS',
        currentUser
      })
      dispatch({
        type: 'DEVICE_SWITCH_EVENT',
        data: device
      })
    })
    .catch(err => {
      dispatch({
        type: 'CURRENT_USER_FAILURE',
        err
      })
    })
}

export const removeDevice = () => (dispatch, getState) => {
  const { currentDevice } = getState().deviceSelection
  const { currentUser } = getState().authenticationRequest
  const index = currentUser.devices.findIndex(d => d.id === currentDevice.id)
  currentUser.devices.splice(index, 1)
  dispatch({
    type: 'CURRENT_USER_SUCCESS',
    currentUser
  })
  dispatch({
    type: 'DEVICE_SWITCH_EVENT',
    data: null
  })
}
