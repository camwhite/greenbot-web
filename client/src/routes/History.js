import React, { Component } from 'react'
import { connect } from 'react-redux'
import Temperature from '../components/Charts/Temperature'
import Humidity from '../components/Charts/Humidity'
import Co2 from '../components/Charts/Co2'
import Range from '../components/Charts/Range'
import Export from '../components/Charts/Export'
import {
  Box,
  Tabs,
  Tab
} from 'grommet'
import Guest from '../components/Onboarding/Guest'
import Owner from '../components/Onboarding/Owner'

class History extends Component {

  render () {
    const { currentDevice, currentUser } = this.props
    if (currentUser.role === 'guest') {
      return <Guest />
    }
    if (currentUser.role === 'owner' && !currentDevice){
      return <Owner />
    }
    return (
      <Box>
        <Range />
        <Tabs justify='center'
          responsive={false}>
          <Tab title='CO2'>
            <Box pad={{ vertical: 'medium' }}
              size={{ horizontal: 'full' }}
              style={{ 'overflowX': 'scroll' }}>
              <Co2 />
            </Box>
          </Tab>
          <Tab title='Temperature'>
            <Box pad={{ vertical: 'medium' }}
              size={{ horizontal: 'full' }}
              style={{ 'overflowX': 'scroll' }}>
              <Temperature />
            </Box>
          </Tab>
          <Tab title='Humidity'>
            <Box pad={{ vertical: 'medium' }}
              size={{ horizontal: 'full' }}
              style={{ 'overflowX': 'scroll' }}>
              <Humidity />
            </Box>
          </Tab>
        </Tabs>
        <Export />
      </Box>
    )
  }
}

const mapStateToProps = (state) => {
  const { currentDevice } = state.deviceSelection
  const { currentUser } = state.authenticationRequest
  return {
    currentDevice,
    currentUser
  }
}

export default connect(mapStateToProps)(History)
