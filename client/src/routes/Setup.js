import React, { Component } from 'react'
import { connect } from 'react-redux'
import Pair from '../components/Wizard/Pair'
import Wifi from '../components/Wizard/Wifi'
import {
  Box,
  Tabs,
  Tab
} from 'grommet'
import { sync, unsync } from '../actions/socket'

class Setup extends Component {

  componentWillMount () {
    this.props.dispatch(sync('device:switch'))
  }

  componentWillUnmount () {
    this.props.dispatch(unsync('device:switch'))
  }

  render () {
    return (
      <Box flex={true}
        pad='large'
        justify='start'
        alignContent='start'
        full={true}>
        <Tabs justify='center'
          responsive={false}>
          <Tab title='Pair Device'>
            <Box align='center'>
              <Pair />
            </Box>
          </Tab>
          <Tab title='WiFi'>
            <Box align='center'>
              <Wifi />
            </Box>
          </Tab>
          <Tab title='Payment'>
            <Box align='center'>
            </Box>
          </Tab>
        </Tabs>
      </Box>
    )
  }
}

export default connect()(Setup)
