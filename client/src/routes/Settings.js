import React, { Component } from 'react'
import Device from '../components/Settings/Device'
import Notifications from '../components/Settings/Notifications'
import {
  Accordion,
  AccordionPanel
} from 'grommet'

class Settings extends Component {
  render () {
    return (
      <Accordion>
        <AccordionPanel heading='Device Settings'>
          <Device />
        </AccordionPanel>
        <AccordionPanel heading='Notification Settings'>
          <Notifications />
        </AccordionPanel>
      </Accordion>
    )
  }
}

export default Settings
