import React, { Component } from 'react'
import { connect } from 'react-redux'
import Co2 from '../components/Sensors/Co2'
import Timelapse from '../components/Sensors/Timelapse'
import Temperature from '../components/Sensors/Temperature'
import Humidity from '../components/Sensors/Humidity'
import Guest from '../components/Onboarding/Guest'
import Owner from '../components/Onboarding/Owner'
import { sync, unsync } from '../actions/socket'
import {
  Box
} from 'grommet'

class Conditions extends Component {

  componentWillMount () {
    this.props.dispatch(sync('sensor:co2'))
    this.props.dispatch(sync('sensor:camera'))
    this.props.dispatch(sync('sensor:dht'))
    this.props.dispatch(sync('sensor:night'))
  }

  componentWillUnmount () {
    this.props.dispatch(unsync('sensor:co2'))
    this.props.dispatch(unsync('sensor:camera'))
    this.props.dispatch(unsync('sensor:dht'))
    this.props.dispatch(unsync('sensor:night'))
  }

  render () {
    const { currentUser, currentDevice } = this.props
    if (currentUser.role === 'guest') {
      return <Guest />
    }
    if (currentUser.role === 'owner' && !currentDevice){
      return <Owner />
    }
    return (
      <Box>
        <Timelapse />
        <Box full='horizontal'
          pad={{ vertical: 'large' }}
          direction='row'
          justify='center'
          wrap={true}
          responsive={true}
          separator='top'>
          <Co2 />
          <Temperature />
          <Humidity />
        </Box>
      </Box>
    )
  }
}

const mapStateToProps = (state) => {
  const { currentUser } = state.authenticationRequest
  const { currentDevice } = state.deviceSelection
  return { currentUser, currentDevice }
}

export default connect(mapStateToProps)(Conditions)
