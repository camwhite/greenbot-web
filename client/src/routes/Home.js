import React, { Component } from 'react'
import { connect } from 'react-redux'
import Guest from '../components/Onboarding/Guest'
import Overview from '../components/Overview/Overview'
import { getUserDevices } from '../actions/device'

class Home extends Component {

  componentWillMount () {
    if (this.props.currentUser.role !== 'guest') {
      this.props.dispatch(getUserDevices())
    }
  }

  render () {
    const { currentUser } = this.props
    if (currentUser.role === 'guest') {
      return <Guest />
    }
    return (
      <Overview />
    )
  }
}

const mapStateToProps = (state) => {
  const { currentUser } = state.authenticationRequest
  const { currentDevice } = state.deviceSelection
  return { currentUser, currentDevice }
}

export default connect(mapStateToProps)(Home)
