export const setSessionIdToken = (token) =>
  sessionStorage.setItem('id_token', token)

export const setLocalIdToken = (token) =>
  localStorage.setItem('id_token', token)

export const getIdToken = () =>
  sessionStorage.getItem('id_token') || localStorage.getItem('id_token')

export const requireAuth = (nextState, replaceState) => {
  if (!getIdToken()) {
    replaceState({ nextPathname: nextState.location.pathname }, '/')
  }
}

const checkStatus = (response) => {
  const { ok, statusText } = response
  if (!ok) {
    throw new Error(statusText)
  }

  return response
}

export const authenticate = (route, credentials) => {
  return fetch(`/auth/${route}`, {
    method: 'post',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then(checkStatus)
    .then(response => response.json())
}

export const fetchCurrentUser = () => {
  const jwt = getIdToken()
  const headers = { 'Authorization': `Bearer ${jwt}` }

  return fetch('/api/users/me', { headers: headers })
    .then(checkStatus)
    .then(response => response.json())
}
