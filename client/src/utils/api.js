import { getIdToken } from './auth'

const checkStatus = (response) => {
  const { ok, statusText } = response
  if (!ok) {
    throw new Error(statusText)
  }

  return response
}

export const callApi = (endpoint, opts={}) => {
  const jwt = getIdToken()
  const headers = {
    'Authorization': `Bearer ${jwt}`,
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  }

  return fetch(`/api/${endpoint}`, { headers: headers, ...opts })
    .then(checkStatus)
    .then(response => response.json())
}
