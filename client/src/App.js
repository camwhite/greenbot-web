import React, { Component } from 'react'
import { BrowserRouter, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import {
  App as GrommetApp,
  Split,
  Box,
  SpinningIcon
} from 'grommet'
import { getCurrentUser } from './actions/auth'
import { connect as socket } from './actions/socket'
import { getIdToken } from './utils/auth'
import './App.css'

// Components
import Auth from './components/Auth/Auth'
import Sidenav from './components/Sidenav/Sidenav'
import Notifications from './components/Notifications/Notifications'

// Routes
import Home from './routes/Home'
import Conditions from './routes/Conditions'
import History from './routes/History'
import Settings from './routes/Settings'
import Setup from './routes/Setup'

class App extends Component {

  componentWillMount () {
    if(getIdToken()) {
      this.props.dispatch(getCurrentUser())
      this.props.dispatch(socket())
    }
  }

  render () {
    const {
      isRequesting,
      isAuthenticated
    } = this.props

    if (isRequesting) {
      return (
        <GrommetApp centered={true}>
          <Box full={true}
            justify='center'
            direction='row'
            align='center'>
            <SpinningIcon size='xlarge' />
          </Box>
        </GrommetApp>
      )
    }
    if (!isAuthenticated) { // check for authentication
      return (
        <GrommetApp>
          <Auth />
          <Notifications />
        </GrommetApp>
      )
    }
    return (
      <BrowserRouter>
        <GrommetApp centered={false}>
          <Split flex='right'
            showOnResponsive='both'>
            <Sidenav />
            <Box pad={{ horizontal: 'large' }}>
              <Route exact path='/' component={Home} />
              <Route path='/conditions' component={Conditions} />
              <Route path='/history' component={History} />
              <Route path='/settings' component={Settings} />
              <Route path='/setup' component={Setup} />
              <Notifications style={{ maxHeight: '100%', overflowY: 'scroll' }} />
            </Box>
          </Split>
        </GrommetApp>
      </BrowserRouter>
    )
  }
}

const mapStateToProps = (state) => {
  const { isRequesting, isAuthenticated } = state.authenticationRequest

  return {
    isRequesting,
    isAuthenticated
  }
}

export default connect(mapStateToProps)(App)
