import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import 'grommet-css'
import registerServiceWorker from './registerServiceWorker'
import configureStore from './store/configureStore'
import { Provider } from 'react-redux'

const store = configureStore()

// Don't forget index import
ReactDOM.render(
<Provider store={store}>
	<App />
</Provider>, document.getElementById('root'))
registerServiceWorker(store)
