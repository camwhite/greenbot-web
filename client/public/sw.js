self.addEventListener('push', (e) => {
  clients.matchAll().then((c) => {
    var payload = e.data.json()
    if (c.length === 0 || c[0] && !c[0].focused) {
      // Show notification
      e.waitUntil(
        self.registration.showNotification(payload.sensor, {
          body: payload.value,
          icon: '/icons/icon-192x192.png',
          vibrate: [200, 100, 200, 100, 200, 100, 200],
          renotify: true,
          tag: payload.device_name
        })
      )
    }
  })
})
