'use strict'

// Main modules
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const passport = require('passport')
const compression = require('compression')
const helmet = require('helmet')

// App modules
const app = express()
const db = require('./models')
const auth = require('./auth')
const router = require('./router')

// Config
const { PUBLIC_PATH } = require('./utils/config')

// Sync the database
db.sequelize.sync()

// Middlewares
if (process.env.NODE_ENV !== 'production') {
  app.use(require('morgan')('dev'))
}
app.use(helmet())
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use(express.static(PUBLIC_PATH))
app.use(passport.initialize())

// Router
app.use('/', router)

// Auth
auth(app)

module.exports = app
