'use strict'

const { Strategy, ExtractJwt } = require('passport-jwt')
const { User } = require('../../models')
const { APP_SECRET } = require('../../utils/config')

module.exports = (passport) => {
  passport.use(new Strategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: APP_SECRET
  },
  (jwtPayload, done) => {
    User.findOne({
      where: { id: jwtPayload.id },
      include: { all: true, nested: false, order: [['created_at', 'DESC']]}
    })
    .then(user => {
      if (user) {
        done(null, user)
      } else {
        done(null, false)
      }
    })
    .catch(err => done(err, null))
  }))
}
