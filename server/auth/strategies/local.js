'use strict'

const { Strategy } = require('passport-local')
const { User } = require('../../models')

/*
 * Email validation helper
 * @param {string} the email to test against
 * @return {boolean} the tested email
 */
const validateEmail = (email) => {
  const pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return pattern.test(email)
}

/*
 * Creates two local strategies, sign up and log in.
 * @param {object} the instance of passport
 * @exports {fucntion} invokes strategies as middleware
 */
module.exports = (passport) => {

  passport.use('local-signup', new Strategy({
    usernameField: 'username',
  },
  (email, password, done) => {
    User.findOne({ where: { email: email }})
      .then(user => {
        if (user) return done(null, false)

        const newUser = User.build({
          email: email,
          password: User.generateHash(password)
        })

        const id = newUser.getDataValue('id')
        newUser.qr_code = User.generateQR(id) // attach qr code

        newUser.save()
          .then(user => done(null, user))
          .catch(err => done(err, null))
      })
      .catch(err => done(err, null))
  }))

  passport.use('local-login', new Strategy({
    usernameField: 'username',
  },
  (email, attempt, done) => {
    User.findOne({
      attributes: { include: ['password'] },
      where: { email: email },
      include: { all: true, nested: false }
    })
      .then(user => {
        if (!user) return done(null, false)
        if (!User.validPassword(attempt, user.password)) {
          return done(null, false)
        }

        done(null, user)
      })
      .catch(err => done(err, null))
  }))

}
