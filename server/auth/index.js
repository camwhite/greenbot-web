'use strict'

const passport = require('passport')
const { User } = require('../models')

module.exports = (app) => {

  // bring in strategies
  require('./strategies/jwt')(passport)
  require('./strategies/local')(passport)

  // signup
  app.post(
    '/auth/signup',
    passport.authenticate('local-signup', {
      session: false
    }),
    (req, res) => {
      const currentUser = req.user.toJSON()
      const payload = { id: currentUser.id } // @TODO increase security within

      currentUser.id_token = User.signJwt(payload)
      currentUser.devices = []

      res.json(currentUser)
    }
  )

  // login
  app.post(
  '/auth/login',
    passport.authenticate('local-login', {
      session: false
    }),
    (req, res) => {
      const currentUser = req.user.toJSON()
      const payload = { id: currentUser.id }

      currentUser.id_token = User.signJwt(payload)

      res.json(currentUser)
    }
  )

  // logout
  app.get('/auth/logout', passport.authenticate('jwt', {
    session: false
  }), (req, res) => {
    if (req.user) {
      req.logout()
    }
    res.redirect('/')
  })
}
