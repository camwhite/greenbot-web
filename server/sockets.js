'use strict'

const chalk = require('chalk')
const redis = require('socket.io-redis')
const sockets = require('socket.io')
const socketioJwt = require('socketio-jwt')
const { CronJob } =require('cron')
const reset = require('./utils/reset')
const { APP_SECRET } = require('./utils/config')
const {
  storeCo2,
  storeTimelapse,
  storeDht,
  tempNotifications,
  humidityNotifications,
  co2Notifications
} = require('./utils/sensors')

class SocketListeners {

  constructor (socket) {
    this.socket = socket
    this.socket.join(socket.decoded_token.id)
    this.socket.on('join', (room) => {
      console.log(chalk.blue(`socket ${socket.decoded_token.id} joined room ${room}`))
      this.socket.join(room)
    })
    this.socket.on('leave', (room) => {
      console.log(chalk.yellow(`socket ${socket.decoded_token.id} left room ${room}`))
      this.socket.leave(room)
    })

    // Event listeners
    // @TODO abstract away repetitive chaining
    this.socket.on('wifi:enable', (data) => {
      this.socket
        .to(this.socket.decoded_token.id)
        .broadcast
        .emit('wifi:enable', data)
    })
    this.socket.on('wifi:scan', (data) => {
      this.socket
        .to(this.socket.decoded_token.id)
        .broadcast
        .emit('wifi:scan', data)
    })
    this.socket.on('wifi:info', (data) => {
      this.socket
        .to(this.socket.decoded_token.id)
        .broadcast
        .emit('wifi:info', data)
    })
    this.socket.on('wifi:success', (data) => {
      this.socket
        .to(this.socket.decoded_token.id)
        .broadcast
        .emit('wifi:success', data)
    })
    this.socket.on('wifi:failure', (data) => {
      this.socket
        .to(this.socket.decoded_token.id)
        .broadcast
        .emit('wifi:failure', data)
    })
    this.socket.on('sensor:calibrate', () => {
      this.socket
        .to(this.socket.decoded_token.id)
        .broadcast
        .emit('sensor:calibrate')
    })
    this.socket.on('device:reset', (deviceId) => {
      this.socket
        .to(this.socket.decoded_token.id)
        .broadcast
        .emit('device:reset')
    })
    this.socket.on('reset:success', async () => {
      await reset(this.socket.decoded_token.id)
      this.socket
        .to(this.socket.decoded_token.id)
        .emit('reset:success')
    })
    this.socket.on('reset:failure', async () => {
      this.socket
        .to(this.socket.decoded_token.id)
        .broadcast
        .emit('reset:failure')
    })
    this.socket.on('sensor:night', () => {
      this.socket.to(this.socket.decoded_token.id).broadcast.emit('sensor:night')
      this.image = null
    })
    this.socket.on('sensor:co2', (data) => {
      this.socket.to(this.socket.decoded_token.id).broadcast.emit('sensor:co2', data)
      if (data) {
        this.co2 = data
        co2Notifications(this.socket, data)
      }
    })
    this.socket.on('sensor:camera', (data) => {
      this.socket.to(this.socket.decoded_token.id).broadcast.emit('sensor:camera', data)
      if (data) {
        this.image = data
      }
    })
    this.socket.on('sensor:dht', (data) => {
      this.socket.to(this.socket.decoded_token.id).broadcast.emit('sensor:dht', data)
      if (data) {
        this.dht = data
        tempNotifications(this.socket, data.temperature)
        humidityNotifications(this.socket, data.humidity)
      }
    })
  }

  async saveDataHourly () {
    console.log(`${this.socket.decoded_token.id} called save data hourly`)
    if (!this.co2 || !this.dht) {
      return
    }
    if (this.image) {
      await storeTimelapse(this.socket.decoded_token.id, this.image)
    }
    await storeCo2(this.socket.decoded_token.id, this.co2)
    await storeDht(this.socket.decoded_token.id, this.dht)
  }
}

const bindListeners = (io) => {
  io.on('connection', (socket) => {
    console.log(chalk.green(`socket ${socket.decoded_token.id} connected`))

    const listeners = new SocketListeners(socket)
    const cron = new CronJob('0 * * * *', async () => await listeners.saveDataHourly())
    cron.start()

    socket.on('disconnect', () => {
      console.log(chalk.red(`socket ${socket.decoded_token.id} disconnected`))
      cron.stop()
    })
  })

  global.io = io
}

module.exports = (server) => {
  const io = sockets(server, { transports: [ 'websocket', 'polling' ] })
  io.use(socketioJwt.authorize({
    secret: APP_SECRET,
    handshake: true
  }))
  io.adapter(redis({ host: 'localhost', port: 6379 }))
  bindListeners(io)
}
