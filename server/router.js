'use strict'

const router = require('express').Router()
const { nocache } = require('./utils/middlewares')
const { PATH_TO_ENTRY } = require('./utils/config')

// Endpoints
require('./endpoints/sensor')(router)
require('./endpoints/user')(router)
require('./endpoints/device')(router)
require('./endpoints/settings')(router)

router.get('/*', nocache(), (req, res) => {
  res.sendFile(PATH_TO_ENTRY)
})

module.exports = router
