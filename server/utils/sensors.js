'use strict'

const webPush = require('web-push')
const {
  Device,
  Co2,
  Temperature,
  Humidity,
  Timelapse,
  Notification
} = require('../models')
const _throttle = require('lodash/throttle')
const opts = { gcmAPIKey: 'AAAAhI3dnnI:APA91bHkg5xLkR8frX3nFfuEZENZrTzxJCeKNVGllybGGdc0kSnfaxxHa4UzImwV8HcxWSySOATv7PnQTbKbvgkP5XM2n9WC9g95xoDDK-oZEagLDr08G5wt9VwVRhAdtFR5KMTR11S_', TTL: 60 }

exports.tempNotifications = _throttle(async (socket, temperature) => {
  const device = await Device.findById(socket.decoded_token.id)
  const owner = await device.getOwner({ attributes: [ 'subscription', 'settings_id' ] })
  const settings = await owner.getSettings()

  let notification
  if (temperature < settings.min_temp || temperature > settings.max_temp) {
    notification = await Notification.create({ sensor: 'temperature', value: Math.round(temperature) })
    await notification.setDevice(device)
  }
  if (notification) {
    socket.to(device.owner_id).emit('sensor:notify', notification.toJSON())
    if (owner.subscription) {
      let payload = notification.toJSON()
      payload = { ...payload, device_name: device.name }
      webPush.sendNotification(
        owner.subscription,
        JSON.stringify(payload),
        opts
      )
    }
  }
}, 1800000)

exports.humidityNotifications = _throttle(async (socket, humidity) => {
  const device = await Device.findById(socket.decoded_token.id)
  const owner = await device.getOwner({ attributes: [ 'subscription', 'settings_id' ] })
  const settings = await owner.getSettings()

  let notification
  if (humidity < settings.min_humidity || humidity > settings.max_humidity) {
    notification = await Notification.create({ sensor: 'humidity', value: Math.round(humidity) })
    await notification.setDevice(device)
  }
  if (notification) {
    socket.to(device.owner_id).emit('sensor:notify', notification.toJSON())
    if (owner.subscription) {
      let payload = notification.toJSON()
      payload = { ...payload, device_name: device.name }
      webPush.sendNotification(
        owner.subscription,
        JSON.stringify(payload),
        opts
      )
    }
  }
}, 1800000)

exports.co2Notifications = _throttle(async (socket, co2) => {
  const device = await Device.findById(socket.decoded_token.id)
  const owner = await device.getOwner({ attributes: [ 'subscription', 'settings_id' ] })
  const settings = await owner.getSettings()

  let notification
  if (co2 < settings.min_co2 || co2 > settings.max_co2) {
    notification = await Notification.create({ sensor: 'co2', value: Math.round(co2) })
    await notification.setDevice(device)
  }
  if (notification) {
    socket.to(device.owner_id).emit('sensor:notify', notification.toJSON())
    if (owner.subscription) {
      let payload = notification.toJSON()
      payload = { ...payload, device_name: device.name }
      webPush.sendNotification(
        owner.subscription,
        JSON.stringify(payload),
        opts
      )
    }
  }
}, 1800000)


exports.handleNotifications = _throttle(async (sensor, socket, data) => {
  const owner = await device.getOwner({ attributes: [ 'subscription', 'settings_id' ] })
  const settings = await owner.getSettings()

  let notification
  if (sensor === 'dht') {
    const { temperature, humidity } = data
    if (temperature < settings.min_temp || temperature > settings.max_temp) {
      notification = await Notification.create({ sensor: 'temperature', value: temperature })
      await notification.setDevice(device)
    }
    if (humidity < settings.min_humidity || humidity > settings.max_humidity) {
      notification = await Notification.create({ sensor: 'humidity', value: humidity })
      await notification.setDevice(device)
    }
  } else {
    if (data < settings.min_co2 || data > settings.max_co2) {
      notification = await Notification.create({ sensor: 'co2', value: data })
      await notification.setDevice(device)
    }
  }

  if (notification) {
    socket.to(device.owner_id).emit('sensor:notify', notification.toJSON())
    if (owner.subscription) {
      const payload = `${notification.sensor} ${notification.value}`
      webPush.sendNotification(
        owner.subscription,
        payload,
        opts
      )
    }
  }
}, 1000)


exports.storeCo2 = async function (deviceId, data) {
  try {
    const device = await Device.findById(deviceId)
    const co2 = await Co2.create({ value: data, device_id: deviceId })
    await device.addCo2(co2.id)
  } catch (err) {
    throw err
  }
}

exports.storeTimelapse = async function (deviceId, data) {
  try {
    const device = await Device.findById(deviceId)
    const timelapse = await Timelapse.create({ value: data, device_id: deviceId })
    await device.addTimelapses(timelapse.id)
  } catch (err) {
    throw err
  }
}

exports.storeDht = async function (deviceId, data) {
  try {
    const device = await Device.findById(deviceId)
    const temperature = await Temperature.create({ value: data.temperature, device_id: deviceId })
    const humidity = await Humidity.create({ value: data.humidity, device_id: deviceId })
    await device.addTemperatures(temperature.id)
    await device.addHumidities(humidity.id)
  } catch (err) {
    throw err
  }
}
