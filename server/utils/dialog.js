'use strict'

const dialogflow = require('dialogflow')
const {
  PROJECT_ID,
  SESSION_ID
} = require('./config').DIALOG

class Dialog {

  constructor (query) {
    this.query = query

    this.sessionClient = new dialogflow.SessionsClient()
    this.sessionPath = this.sessionClient.sessionPath(PROJECT_ID, SESSION_ID)

    this.request = {
      session: this.sessionPath,
      queryInput: {
        text: {
          text: this.query,
          languageCode: 'en-US',
        }
      }
    }
  } 

  async analyzeIntent () {
    let responses
    try {
      responses = await this.sessionClient.detectIntent(this.request)
    } catch (err) {
      throw err
    }
    const { intent, fulfillmentText } = responses[0].queryResult

    return {
      isDefault: intent && intent.displayName === 'Default Fallback Intent',
      text: fulfillmentText
    }
  }

}

module.exports = Dialog
