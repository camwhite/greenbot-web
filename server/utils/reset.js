'use strict'

const { Device } = require('../models')

module.exports = async (id) => {
  const device = await Device.findById(id)
  await device.destroy()
}
