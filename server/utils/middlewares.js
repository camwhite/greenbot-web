'use strict'

class Middlewares {
  static nocache () {
    return (req, res, next) => {
      res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate')
      res.header('Expires', '-1')
      res.header('Pragma', 'no-cache')
      next()
    }
  }
  static ensureAuthenticated () {
    return (req, res, next) => {
      if (req.isAuthenticated()) {
        return next()
      }
      res.status(401).redirect('/')
    }
  }
  static checkUserAgent () {
    return (req, res, next) => {
      if (req.headers['user-agent'] === 'Greenbot Pi') {
        return next()
      }
      res.sendStatus(400)
    }
  }
  static protectEndpoint () {
    return (req, res, next) => {
      if (req.isAuthenticated() && req.user.role === 'owner') {
        return next()
      }
      res.sendStatus(403)
    }
  }
}

module.exports = Middlewares
