'use strict'

const path = require('path')

const isProd = process.env.NODE_ENV == 'production'
const publicPath = path.join(__dirname, '../../client/build/')
// @TODO properly handle db auth
const db = {
  URL: isProd ? 'greenbot-prod' : 'greenbot-dev',
  USER: isProd ? 'postgres' : 'camwhite',
  PASSWORD: isProd ? 'Car0XiCi1b1w3NjKO7%Ks' : null,
  OPTIONS: {
    host: 'localhost',
    dialect: 'postgres',
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
    logging: false
  }
}

const dialog = {
  PROJECT_ID: 'greenbot-9b115',
  SESSION_ID: require('uuid/v1')()
}

module.exports = {
  APP_SECRET: process.env.SECRET || 'winston\'s the one',
  PUBLIC_PATH: publicPath,
  PATH_TO_ENTRY: path.join(publicPath, 'index.html'),
  DIALOG: dialog,
  DB: db
}
