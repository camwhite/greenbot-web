'use strict'

module.exports = (sequelize, DataTypes) => {
  const Settings = sequelize.define(
    'Settings',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      is_fahrenheit: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      min_temp: {
        type: DataTypes.INTEGER,
        defaultValue: 15
      },
      max_temp: {
        type: DataTypes.INTEGER,
        defaultValue: 35
      },
      min_co2: {
        type: DataTypes.INTEGER,
        defaultValue: 300
      },
      max_co2: {
        type: DataTypes.INTEGER,
        defaultValue: 1500
      },
      min_humidity: {
        type: DataTypes.INTEGER,
        defaultValue: 25
      },
      max_humidity: {
        type: DataTypes.INTEGER,
        defaultValue: 60
      },
    },
    {
      underscored: true
    }
  )

  return Settings
}
