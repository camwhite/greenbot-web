'use strict'

const qr = require('qr-image')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const {
  APP_SECRET
} = require('../utils/config')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      qr_code: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          isEmail: true
        }
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      role: { 
        type: DataTypes.ENUM('guest', 'owner', 'subscriber'),
        defaultValue: 'guest'
      },
      subscription: DataTypes.JSON
    },
    {
      underscored: true,
      defaultScope: {
        attributes: { exclude: [ 'password' ] }
      }
    }
  )

  User.generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
  }

  User.validPassword = (attempt, password) => {
    return bcrypt.compareSync(attempt, password)
  }

  User.signJwt = (payload) => {
    return jwt.sign(payload, APP_SECRET) // @TODO increase security within
  }

  User.generateQR = function (id) {
    return qr.imageSync(this.signJwt({ id }), { type: 'svg' })
  }

  User.associate = ({ Device, Settings }) => {
    User.belongsTo(Settings, { as: 'settings' })
    User.belongsToMany(Device, { as: 'devices', through: 'UserDevices' })
  }

  return User
}
