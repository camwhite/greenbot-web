'use strict'

module.exports = (sequelize, DataTypes) => {
  const Timelapse = sequelize.define(
    'Timelapse',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      value: {
        type: DataTypes.BLOB,
        allowNull: false
      }
    },
    {
      underscored: true
    }
  )

  Timelapse.associate = ({ Device }) => {
    Timelapse.belongsTo(Device, { as: 'device' })
  }

  return Timelapse
}
