'use strict'

module.exports = (sequelize, DataTypes) => {
  const Temperature = sequelize.define(
    'Temperature',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      value: {
        type: DataTypes.DECIMAL,
        allowNull: false
      }
    },
    {
      underscored: true
    }
  )

  Temperature.associate = ({ Device }) => {
    Temperature.belongsTo(Device, { as: 'device' })
  }

  return Temperature
}
