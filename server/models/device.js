'use strict'

const jwt = require('jsonwebtoken')
const {
  APP_SECRET
} = require('../utils/config')

module.exports = (sequelize, DataTypes) => {
  const Device = sequelize.define(
    'Device',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      serial_number: {
        type: DataTypes.STRING,
        allowNull: false
      },
      name: DataTypes.STRING,
      description: DataTypes.TEXT
    },
    {
      underscored: true
    }
  )

  Device.signJwt = (payload) => {
    return jwt.sign(payload, APP_SECRET) // @TODO increase security within
  }

  Device.associate = ({ User, Temperature, Humidity, Timelapse, Co2 }) => {
    Device.belongsTo(User, { as: 'owner' })
    Device.belongsToMany(Co2, { as: 'co2', through: 'DeviceCo2s' })
    Device.belongsToMany(Temperature, { as: 'temperatures', through: 'DeviceTemperatures' })
    Device.belongsToMany(Humidity, { as: 'humidities', through: 'DeviceHumidities' })
    Device.belongsToMany(Timelapse, { as: 'timelapses', through: 'DeviceTimelapses' })
  }

  return Device
}
