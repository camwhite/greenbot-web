'use strict'

module.exports = (sequelize, DataTypes) => {
  const Humidity = sequelize.define(
    'Humidity',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      value: {
        type: DataTypes.DECIMAL,
        allowNull: false
      }
    },
    {
      underscored: true
    }
  )

  Humidity.associate = ({ Device }) => {
    Humidity.belongsTo(Device, { as: 'device' })
  }

  return Humidity
}
