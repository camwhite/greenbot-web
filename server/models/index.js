'use strict'

const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const Sequelize = require('sequelize')
const config = require('../utils/config')

const db = {}
const logger = (log) => {
  console.log(chalk.blue(log))
}

const { URL, USER, PASSWORD, OPTIONS } = config.DB
const sequelize = new Sequelize(
  URL,
  USER,
  PASSWORD,
  OPTIONS
)

db['sequelize'] = sequelize

fs.readdirSync(__dirname)
  .filter(file => file.indexOf('.') != 0 && file != 'index.js')
  .forEach(file => {
    const model = sequelize.import(path.join(__dirname, file))
    db[model.name] = model
  })

Object.keys(db)
  .forEach(modelName => {
    if ('associate' in db[modelName]) {
      db[modelName].associate(db)
    }
  })

module.exports = db
