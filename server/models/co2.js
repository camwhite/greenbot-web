'use strict'

module.exports = (sequelize, DataTypes) => {
  const Co2 = sequelize.define(
    'Co2',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV1,
        primaryKey: true
      },
      value: {
        type: DataTypes.FLOAT,
        allowNull: false
      }
    },
    {
      underscored: true
    }
  )

  Co2.associate = ({ Device }) => {
    Co2.belongsTo(Device, { as: 'device' })
  }

  return Co2
}
