'use strict'

const passport = require('passport')
const { me, update, ask } = require('./controller')
const { ensureAuthenticated } = require('../../utils/middlewares')

module.exports = (router) => {

  router.get(
   '/api/users/me',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    me // call the controller's method
  )

  router.post(
  '/api/users/ask',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    ask // call the controller's method
  )

  router.put(
  '/api/users',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    update // call the controller's method
  )

}
