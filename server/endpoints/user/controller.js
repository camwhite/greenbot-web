'use strict'

const Dialog = require('../../utils/dialog')
const { User } = require('../../models')

class Controller {

  // Get the current user
  static me (req, res) {
    res.json(req.user)
  }

  // Update the current user
  static async update (req, res) {
    let updated
    try {
      updated = await req.user.update(req.body)
    } catch (err) {
      res.send(err).status(500)
    }

    res.json(updated)
  }

  // Ask greenbot a question
  static async ask (req, res) {
    const dialog = new Dialog(req.body.query)

    let response
    try {
      response = await dialog.analyzeIntent()
    } catch (err) {
      res.send(err).status(500)
    }

    res.json({
      text: response.text,
      status: response.isDefault ? 'warning' : 'unknown',
      state: 'Greenbot'
    })
  }

}

module.exports = Controller
