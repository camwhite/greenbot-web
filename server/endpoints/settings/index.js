'use strict'

const passport = require('passport')
const { update } = require('./controller')
const {
  ensureAuthenticated
} = require('../../utils/middlewares')

module.exports = (router) => {

  router.put(
    '/api/settings/:id',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    update // call the controller's method
  )


}
