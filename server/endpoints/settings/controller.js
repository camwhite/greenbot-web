'use strict'

const { Settings, User } = require('../../models')

User.hook('afterCreate', async (user, options) => {
  const settings = await Settings.create()
  await user.setSettings(settings)
})

class Controller {

  static async update (req, res) {
    const { id } = req.params

    let settings
    try {
      settings = await Settings.findById(id)
    } catch (err) {
      res.send(err).status(500)
    }
    const updated = await settings.update(req.body)

    res.json(updated)
  }

}

module.exports = Controller
