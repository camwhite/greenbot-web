'use strict'

const passport = require('passport')
const { all, associate, verify, readings, update } = require('./controller')
const {
  ensureAuthenticated,
  checkUserAgent
} = require('../../utils/middlewares')

module.exports = (router) => {

  router.post(
   '/api/devices',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    checkUserAgent(),
    associate // call the controller's method
  )

  router.put(
  '/api/devices/:id',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    update // call the controller's method
  )

  router.get(
    '/api/devices',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    checkUserAgent(),
    verify // call the controller's method
  )

  router.get(
    '/api/devices/all',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    all // call the controller's method
  )

  router.get(
    '/api/devices/readings',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    readings // call the controller's method
  )

}
