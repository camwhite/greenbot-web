'use strict'

const { Device, Co2, User, Temperature, Humidity, Timelapse } = require('../../models')

class Controller {

  static async all (req, res) {
    let devices, repsonse
    try {
      devices = await Device.findAll({
        where: { owner_id: req.user.id },
        raw: true
      })
      for (const [index, device] of devices.entries()) {
        const timelapse = await Timelapse.findOne({
          where: { device_id: device.id },
          order: [[ 'created_at', 'DESC' ]]
        })
        devices[index].timelapse = timelapse
      }
    } catch (err) {
      return res.send(err).status(500)
    }

    res.json(devices)
  }

  static async readings (req, res) {
    let device
    try {
      device = await Device.findById(req.query.device, {
        include: [
          {
            model: Temperature,
            as: 'temperatures'
          },
          {
            model: Humidity,
            as: 'humidities'
          }
        ]
      })
    } catch (err) {
      res.send(err).status(500)
    }

    res.json(device)
  }

  // Tie a device to a user acct
  static async associate (req, res) {
    let device
    try {
      device = await Device.create({
        serial_number: req.body.serialNumber
      })
    } catch (err) {
      res.send(err).status(500)
    }
    req.user.role = 'owner'
    try {
      await device.setOwner(req.user)
      await req.user.addDevices(device)
      await req.user.save()
    } catch (err) {
      res.send(err).status(500)
    }
    global.io.to(req.user.id).emit('device:switch', device.toJSON()) // pass a verify event over the socket
    const id_token = Device.signJwt({ id: device.id })

    res.json({
      ...device.toJSON(),
      id_token
    })
  }

  // Update a device
  static async update (req, res) {
    const { id } = req.params

    let device
    try {
      device = await Device.findById(id)
    } catch (err) {
      res.send(err).status(500)
    }
    const updated = await device.update(req.body)

    res.json(updated)
  }

  // Verify a device belongs to a user
  static async verify (req, res) {
    const devices = await Device.findAll({
      where: { owner_id: req.user.id }
    })

    if (devices) {
      res.json(devices)
    } else {
      res.sendStatus(403)
    }
  }

}

module.exports = Controller
