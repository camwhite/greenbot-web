'use strict'

const passport = require('passport')
const { readings, timelapse } = require('./controller')
const { ensureAuthenticated } = require('../../utils/middlewares')

module.exports = (router) => {

  router.get(
    '/api/sensors/readings',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    readings // call the controller's method
  )

  router.get(
  '/api/sensors/timelapse',
    passport.authenticate('jwt', {
      session: false
    }),
    ensureAuthenticated(),
    timelapse // call the controller's method
  )

}
