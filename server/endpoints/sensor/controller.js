'use strict'

const { Device, Temperature, Humidity, Co2, Timelapse } = require('../../models')

class Controller {

  static async readings (req, res) {
    const { deviceId, startDate, endDate } = req.query
    let temperatures, humidities, co2s
    try {
      temperatures = await Temperature.findAll({
        where: {
          device_id: deviceId,
          created_at: { $between: [ startDate, endDate ]}
        },
        order: [['created_at', 'ASC']]
      })
      humidities = await Humidity.findAll({
        where: {
          device_id: deviceId,
          created_at: { $between: [ startDate, endDate ]}
        },
        order: [['created_at', 'ASC']]
      })
      co2s = await Co2.findAll({
        where: {
          device_id: deviceId,
          created_at: { $between: [ startDate, endDate ]}
        },
        order: [['created_at', 'ASC']]
      })
    } catch (err) {
      res.send(err).status(500)
    }

    res.json({ temperatures, humidities, co2s })
  }

  static async timelapse (req, res) {
    const { createdAt, deviceId } = req.query

    const startRange = new Date(createdAt).setSeconds(-1)
    const endRange = new Date(createdAt).setSeconds(59)
    const startTime = new Date(startRange)
    const endTime = new Date(endRange)

    let timelapse
    try {
      timelapse = await Timelapse.findOne({
        where: {
          device_id: deviceId,
          created_at: {
            $between: [ startTime, endTime ]
          }
        }
      })
    } catch (err) {
      res.send(err).status(500)
    }

    if (!timelapse) {
      return res.sendStatus(404)
    }

    res.json(timelapse)
  }

}

module.exports = Controller
